// impport thư viện mongose
const mongoose = require("mongoose");

//khai báo thư viện class Schema của mongoose
const Schema = mongoose.Schema;

//Khai báo review schema
const reviewSchema = new Schema({
   /* _id:{
        type : mongoose.Types.ObjectId,
        required : true
    },*/
    stars : {
        type: Number,
        default: 0
    },
    note : {
        type : String,
        required : false
    }
},{
    timestamps : true
});

module.exports = mongoose.model("reviews",reviewSchema);