// impport thư viện mongose
const mongoose = require("mongoose");

// khai báo class Schema của thư viện mongoseJs
const Schema = mongoose.Schema;

// Khai báo course Schema
const courseSchema = new Schema({
    title : {
        type : String,
        required : true,
        unique : true
    },
    description : {
        type : String,
        required : false
    },
    noStudent :{
        type : Number,
        default : 0
    },
    reviews: [
        {
            type: mongoose.Types.ObjectId,
            ref:"reviews"
        }
    ]
},{
    timestamps : true
})

module.exports = mongoose.model("courses", courseSchema)