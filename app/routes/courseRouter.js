const express = require('express');

const router = express.Router();

router.get("/courses", (request, response) => {
    response.status(200).json({
        message: "GET ALL COURSE"
    })
});

router.get("/courses/:courseId", (request, response) => {
   let courseId = request.params.courseId;

    response.status(200).json({
        message: "GET Course Id = " + courseId
    })
});

router.post("/courses", (request, response) => {
    response.status(200).json({
        message: "Create Course"
    })
});

router.put("/courses/:courseId", (request, response) => {
    let courseId = request.params.courseId;
 
     response.status(200).json({
         message: "Update Course Id = " + courseId
     })
 });

 router.delete("/courses/:courseId", (request, response) => {
    let courseId = request.params.courseId;
 
     response.status(200).json({
         message: "Delete Course Id = " + courseId
     })
 });

 module.exports = router;