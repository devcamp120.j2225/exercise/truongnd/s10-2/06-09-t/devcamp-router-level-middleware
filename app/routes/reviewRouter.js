const express = require('express');

const router = express.Router();

router.get("/reviews" ,(req,res) => {
    res.status(200).json({
        message:"Get All Review"
    });
})

router.get("/reviews/:reviewId" ,(req,res) => {
    let reviewId = req.params.reviewId;

    res.status(200).json({
        message:"Get Review Id = " + reviewId
    });
})

router.post("/reviews" ,(req,res) => {
    res.status(200).json({
        message:"Create Review"
    });
})

router.put("/reviews/:reviewId" ,(req,res) => {
    let reviewId = req.params.reviewId;

    res.status(200).json({
        message:"Put Review Id = " + reviewId
    });
})

router.delete("/reviews/:reviewId", (req,res) => {
    let reviewId = req.params.reviewId;

    res.status(200).json({
        message:"Delete Review Id = " + reviewId
    })
})

module.exports = router;